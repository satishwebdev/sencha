# dst-theme/sass/etc

This folder contains miscellaneous SASS files. Unlike `"dst-theme/sass/etc"`, these files
need to be used explicitly.
