Ext.define("dst.view.header.Employee", {
    extend: 'Ext.container.Container',
    alias: 'widget.employeeHeader',
    layout: 'fit',
    style: {
        height: 45
    },
    cls: 'headerClass',
    initComponent: function(){
        this.items = {
            xtype: 'container',
            layout: 'column',
            items: [{
                xtype: 'container',
                columnWidth: '0.50',
                items:[{
                   xtype: 'container',
                   layout:'hbox',
                   items: [{
                       xtype: 'container',
                       width: 30,
                       html: '<div ></div>'
                   },{
                       xtype: 'container',
                       width: '50%',
                       html: "<div style='line-height:45px;'><span style='font-weight:bold; font-size:19px;' >Census:</span> <span style='font-size:16px;' > Employee Detail</span></div>"
                   }]
                }]
            },{
                xtype: 'container',
                columnWidth: '0.50',
                items:[{
                   xtype: 'container',
                   layout:'hbox',
                   items: [{
                       xtype: 'container',
                       width: '10%',
                       html: "<div></div>"
                   },{
                       xtype: 'container',
                       width: '90%',
                       html: "<div style='display:block;width:290px;float:right;margin-right:35px;'><div style='position:relative;top:8px;line-height:18px;border-right:2px solid #ccc;display:inline-block;text-align:right;padding-right:10px;'><span style='position:relative;'>ABC Trucking Company</span><br><span style='position:relative;top:-4px;'>Plan - ID000001</span></div>\n\
                        <div style='color:#0066cc;line-height:45px;position:absolute;top:0px;display:inline-block;padding-left:10px;' >Helpful Documents<span class='downArrow'>&nbsp;</span></div></div>"
                   }]
                }]
            }]
        };
        this.callParent();
    }
});