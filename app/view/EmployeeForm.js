Ext.define("dst.view.EmployeeForm", {
    extend: 'Ext.container.Viewport',
    alias : 'widget.employeeform',
    requires: [
        'Ext.grid.*',
        'Ext.form.*',
        'Ext.tip.*',
        'Ext.layout.container.Column',
        'dst.model.Employees',
        'dst.view.header.Employee',
        'dst.view.footer',
        'dst.view.Employee.personal',
        'dst.view.Employee.compensation',
        'dst.view.Employee.hours',
        'dst.view.Employee.ownership',
        'dst.view.Employee.relationship',
        'dst.view.Employee.employment',
        'dst.view.Employee.attributes',
        'dst.view.Employee.contributions',
        'dst.view.Employee.button'
    ],
//    autoScroll: true,
    layout: {
        type: 'auto'
    },
    style: {
        overflowX: 'hidden',
        overflowY: 'auto'
    },
    defaults: {
        frame: false
    },
    initComponent: function(){
//        Ext.Msg.alert('Status', 'Changes saved successfully.');
        this.items = {
            xtype: 'panel',
            dockedItems: [{
//                dock: 'top',
//                xtype: 'panel',
//                style: {
//                    background: '#ccc'
//                },
//                height: 45,
//                items: [{
//                    xtype: 'employeeHeader'
//                }]
//            },{
                dock: 'bottom',
                xtype: 'panel',
                height: 36,
//                padding: '0 100',
                border: 1,
                items: [{
                    padding: '5 30',
                    layout: 'fit',
                    xtype: 'footer'
                }]
            }],
            items:[{
                height: 45,
                flex: 1,
                layout: 'fit',
                region: 'north',
                xtype: 'employeeHeader',
                border: 1
            },{
                flex: 1,
                xtype: 'panel',
                layout: {
                    type: 'vbox',
                    align: 'stretch',
                    padding: '0 30'
                },
                items: [{
                   padding: '5 0 10 0',     
                   html: "<div style='display:block;text-align:right;margin-right:20px;'>ABC Trucking Company | 01/01/2012 thru 12/13/2012 | Year-end</div>"
                },{
                    html: "<div style='width:984px;margin-right:20px;background:#fff9f2; border:1px solid #fb9005; border-radius:5px;display:block;padding:10px;color:#b87a41;'><span class='errorExclamationImg'>&nbsp;</span>Employment Hire Date is required.</div>"
                }]
            },{
                layout: {
                    type: 'hbox',
                    padding: '0 20'
                },
                style: {
                    width: '1024px'
                },
                items: [{
                    flex: 1,
                    xtype: 'panel',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Personal</span></div>"
                        },{    
                            xtype: 'personal',
                            height: 130
                        }]
                    }, {
                        layout: 'fit',
                        html: "<div style='border-bottom:1px solid #CCC;width:100%;position:relative;left:10px;'></div>"
                    }, {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Compensation Types and Amounts</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{ 
                            xtype: 'compensation',
                            height: 130
                        }]
                    }, {
                        layout: 'fit',
                        html: "<div style='border-bottom:1px solid #CCC;width:100%;position:relative;left:10px;'></div>"
                    }, {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 0 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Hours</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{
                            xtype: 'hours',
                            height: 80
                        }]
                    }, {
                        layout: 'fit',
                        html: "<div style='border-bottom:1px solid #CCC;width:100%;position:relative;left:10px;'></div>"
                    }, {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Ownership</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{
                            xtype: 'ownership',
                            height: 75
                        }]
                    }, {
                        layout: 'fit',
                        html: "<div style='border-bottom:1px solid #CCC;width:100%;position:relative;left:10px;'></div>"
                    }, {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout: 'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Employee Relationship</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{
                            padding: '10 0 0 10',
                            xtype: 'relationship',
                            height: 100        
                        },{
                            html: "<div style='text-align:right;padding:top:2px;'><span style='cursor:pointer;color:#0066cc;font-weight:bold;font-size:13px;padding-right:5px;'>+&nbsp;Add Another</span></div>"
                        }]
                    }]
                }, {
                    xtype: 'container',
                    width: 20,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    }
                },{
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Employment</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{    
                            xtype: 'employment',
                            height: 270
                        }]
                    }, {
                        layout: 'fit',
                        html: "<div style='border-bottom:1px solid #CCC;width:100%;position:relative;left:10px;'></div>"
                    }, {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Employee Attributes</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{
                            padding: '0 0 0 10',
                            xtype: 'attributes',
                            height: 160
                        }]
                    }, {
                        layout: 'fit',
                        html: "<div style='border-bottom:1px solid #CCC;width:100%;position:relative;left:10px;'></div>"
                    }, {
                        flex: 1,
                        xtype: 'panel',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            layout:'fit',
                            padding: '20 0 7 10',
                            html: "<div><span style='font-weight:bold;font-size:13px;padding-right:5px;'>Contributions and Balances</span>\n\
                            |<span class='field-defination' data-qtip='Lorem Ipsum is simply dummy text of the printing and typesetting industry.'>Field Definations</span></div>"
                        },{
                            padding: '10 0 20 10',
                            xtype: 'contributions',
                            height: 280
                        },{
                            html: "<div>\n\
                            <span style='float:left;font-weight:bold;font-size:13px;padding-left:22px;'>Totals:</span>\n\
                            <span style='position:absolute;right:38px;font-weight:bold;font-size:13px;padding-right:15px;'>$36.050.00</span>\n\
                            </div>"
                        }]
                    }]
                }]
            },{
                layout: {
                    type: 'fit'
                },
                style: {
                    width: '1024px'
                },
                items: [{
                    padding: '20 10 0 30',
                    xtype: 'employeeButton',
                    height: 80
                }]
            }]    
            
        };
        this.callParent();
    }
});
