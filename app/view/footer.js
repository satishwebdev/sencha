Ext.define("dst.view.footer", {
    extend: 'Ext.Component',
    alias: 'widget.footer',
    layout: 'fit',
    html: "<div style='display:block;'>DST Copyright 2014 | Disclaimer</div>"
});