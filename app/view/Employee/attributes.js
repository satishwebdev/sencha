Ext.define("dst.view.Employee.attributes", {
    extend: 'Ext.form.Panel',
    alias: 'widget.attributes',
//    title: 'Employee Attributes',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'right',
        boxLabelAlign: 'after',
        width: 100
    },
    initComponent: function(){
        this.items = [
            {
                layout: 'vbox',
                items: [{
                    layout: 'hbox',
                    items: [{
                        xtype: 'checkboxfield',
                        name : 'hce',
                        boxLabel: 'HCE',
                        checked   : true,
                        width: 130
                    },{
                        xtype: 'checkboxfield',
                        name : 'seasonal',
                        boxLabel: 'Seasonal',
                        width: 180
                    },{
                        xtype: 'checkboxfield',
                        name : 'union',
                        boxLabel: 'Union'
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'checkboxfield',
                        name : 'key',
                        boxLabel: 'KEY',
                        checked   : true,
                        width: 130
                    },{
                        xtype: 'checkboxfield',
                        name : 'nonResidentAlian',
                        boxLabel: 'Non-resident Alien',
                        width: 180
                    },{
                        xtype: 'checkboxfield',
                        name : 'contractor',
                        boxLabel: 'Contractor'
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'checkboxfield',
                        name : 'officer',
                        boxLabel: 'Officer',
                        checked   : true,
                        width: 130
                    },{
                        xtype: 'checkboxfield',
                        name : 'partTimeEmployee',
                        boxLabel: 'Part-time Employee',
                        width: 180
                    },{
                        xtype: 'checkboxfield',
                        name : 'hourly',
                        boxLabel: 'Hourly'
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'Prior Key',
                        labelAlign: 'left',
                        width: 80
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'checkboxfield',
                        name : 'year1',
                        boxLabel: '2011',
                        checked   : true
                    },{
                        xtype: 'checkboxfield',
                        name : 'year2',
                        boxLabel: '2010',
                        checked   : true
                    },{
                        xtype: 'checkboxfield',
                        name : 'year3',
                        boxLabel: '2009',
                        checked   : true
                    },{
                        xtype: 'checkboxfield',
                        name : 'year3',
                        boxLabel: '2008',
                        checked   : true
                    }]
                }]
            }
        ];
        this.callParent();
    }
});