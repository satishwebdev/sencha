Ext.define("dst.view.Employee.compensation", {
    extend: 'Ext.form.Panel',
    alias: 'widget.compensation',
    layout: 'fit',
//    title: 'Compensation Types and Amounts',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'top',
        width: 120
    },
    border: false,
    autoHeight: true,
    bodyPadding: '0 10',
    initComponent: function(){
        this.items = [
            {
                layout: 'vbox',
                items: [{
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'textfield',
                            name : 'limitation1',
                            fieldLabel: '2011 Limitation',
                            value: '175,000',
                            padding: '0 20 0 0'
                        },{
                            xtype: 'textfield',
                            name : 'limitation2',
                            fieldLabel: '2012 Limitation',
                            value: '175,000'
                        }
                    ]
                },{
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'textfield',
                            name : 'planYear',
                            fieldLabel: '2012 Plan Year',
                            padding: '0 20 0 0',
                            value: '175,000'
                        },{
                            xtype: 'textfield',
                            name : 'allocation',
                            fieldLabel: '2012 Allocation',
                            value: '175,000'
                        }
                    ]
                }]
            }
        ];
        this.callParent();
    }
});
