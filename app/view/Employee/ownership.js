Ext.define("dst.view.Employee.ownership", {
    extend: 'Ext.form.Panel',
    alias: 'widget.ownership',
    layout: 'fit',
//    title: 'Ownership',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'top',
        width: 120
    },
    border: false,
    autoHeight: true,
    bodyPadding: '0 10',
    initComponent: function(){
        this.items = [
            {
                layout: 'vbox',
                items: [{
                    layout: 'hbox',
                    items: [{
                        xtype: 'textfield',
                        name : 'year1',
                        fieldLabel: '2011',
                        value: 4,
                        padding: '0 20 0 0'
                    },{
                        xtype: 'textfield',
                        name : 'year2',
                        fieldLabel: '2012',
                        minValue: 0
                    }]
                }]
            }
        ];
        this.callParent();
    }
});