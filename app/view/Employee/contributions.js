Ext.define("dst.view.Employee.contributions", {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contributions',
//    title: 'Contributions and Balances',
    initComponent: function(){
        this.height = 193,
        this.store = {
                        fields: ['type', 'amount'],
                        data: [
                            {
                                type: 'Elective Deferral',
                                amount: '$35,000.00'
                            },{
                                type: 'Catch-up Elective Deferral',
                                amount: '$0.00'
                            },{
                                type: 'Employer Matching',
                                amount: '$1,050.00'
                            },{
                                type: 'Profit Sharing',
                                amount: '$0.00'
                            },{
                                type: 'Voluntary After-tax',
                                amount: '$0.00'
                            },{
                                type: 'Elective Deferral Copy',
                                amount: '$35,000.00'
                            },{
                                type: 'Catch-up Elective Deferral Next',
                                amount: '$0.00'
                            },{
                                type: 'Employer Matching Second',
                                amount: '$1,050.00'
                            },{
                                type: 'Profit Sharing Duplicate',
                                amount: '$0.00'
                            },{
                                type: 'Voluntary After-taxxx',
                                amount: '$0.00'
                            }
                        ]
        };
        this.columns = [
            {
                id: 'type',
                header: 'Type',
                dataIndex: 'type',
                width: '80%'
            },{
                id: 'amount',
                header: 'Amount',
                dataIndex: 'amount',
                flex: 1,
                editor: {
                    allowBlank: false
                },
                width: '20%',
                cls: 'contributionAmountColumn'
            }
        ];
        this.plugins = [
            Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1
            }),
            {
                ptype: 'bufferedrenderer',
                trailingBufferZone: 6,  
                leadingBufferZone: 8   
            }
        ];
        this.callParent(arguments);
    }
});