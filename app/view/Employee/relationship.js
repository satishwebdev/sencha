Ext.define("dst.view.Employee.relationship", {
    extend: 'Ext.grid.Panel',
    alias: 'widget.relationship',
//    title: 'Employee Relationship',
//    bodyPadding: '0 10',
//    store: [],
//    columns: [],
    initComponent: function() {
        this.store = {
            fields: ['ssn', 'firstName', 'middleName', 'lastName', 'relationship'],
            data: [
                {
                    ssn: 'XXX-XX-5004',
                    firstName: 'John',
                    middleName: 'W',
                    lastName: 'Smith',
                    relationship: 'Spouse'
                },{
                    ssn: '',
                    firstName: '',
                    middleName: '',
                    lastName: '',
                    relationship: ''
                }
            ]
        }; 
        this.columns = [
            {
                id: 'ssn',
                header: 'SSN',
                dataIndex: 'ssn',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                id: 'firstName',
                header: 'First Name',
                dataIndex: 'firstName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                id: 'middleName',
                header: 'MI',
                dataIndex: 'middleName',
                width: 50,
                editor: {
                    allowBlank: false
                }
            }, {
                id: 'lastName',
                header: 'Last Name',
                dataIndex: 'lastName',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: 'Relationship',
                dataIndex: 'relationship',
                width: 100,
                editor: {
                    xtype: 'combobox',
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnTab: true,
                    store: [
                        ['Spouse','Spouse'],
                        ['Son','Son'],
                        ['Daughter','Daughter'],
                        ['Father','Father'],
                        ['Mother','Mother']
                    ],
                    lazyRender: true,
                    listClass: 'x-combo-list-small'
                }
            },
            {
            xtype: 'actioncolumn',
            width: '10',
            items: [ {
                icon: 'resources/images/delete.png',
                tooltip: 'Delete',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    alert("Delete " + rec.get('lastName'));
                }
            }
            ]
        }
        ];
        this.plugins = [
            Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1
            })
        ];
        this.callParent(arguments);
    }
});