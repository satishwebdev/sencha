Ext.define("dst.view.Employee.button", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.employeeButton',
    layout: 'fit',
    flex: '1',
//    height: 60,
    initComponent: function () {
        this.items = {
            xtype: 'container',
            layout: 'column',
            items: [{
                xtype: 'container',
                columnWidth: '0.50',
                items:[{
                   layout:'hbox',
                   items: [{
                       xtype: 'panel',
                       width: '50%',
                       items: [{
                                xtype: 'container',
                                html: '<span>Done</span>',
                                cls: 'buttonDone'
                           }]
                      }]
                }]
            },{
                xtype: 'panel',
                columnWidth: '0.50',
                items:[{
                   layout:'hbox',
                   items: [{
                       xtype: 'container',
                       width: '30%',
                       html: "<div></div>"
                   },{
                       layout: 'hbox',
                       width: '70%',
                       items: [{
                          width: '50%',
                          xtype: 'container',
                          cls: 'buttonPrv',
                          html: '<span>< Previous Employee</span>'
                       },{
                          width: '50%',
                          xtype: 'container',
                          cls: 'buttonNxt',
                          html: '<span>Next Employee ></span>'
                       }]
                   }]
                }]
            }]
        };
        this.callParent();
    }
});