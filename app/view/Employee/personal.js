Ext.define("dst.view.Employee.personal", {
    extend: 'Ext.form.Panel',
    alias: 'widget.personal',
    layout: 'fit',
//    title: 'Personal Information',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'top',
        width: 200
    },
    border: false,
    autoHeight: true,
    bodyPadding: '0 10',
    defaults: {
        anchor: '100%',
        labelWidth: 100
    },
    initComponent: function(){
        this.items = [
            {
                layout: 'vbox',
                items: [{
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'textfield',
                                name : 'lastName',
                                fieldLabel: 'Last Name',
                                value: 'Smith',
                                padding: '0 15 0 0'
                            },{
                                xtype: 'textfield',
                                name : 'firstName',
                                fieldLabel: 'First Name',
                                value: 'Jane',
                                padding: '0 15 0 0'
                            },{
                                xtype: 'textfield',
                                name : 'middleName',
                                fieldLabel: 'MI',
                                width: 30,
                                value: 'A'
                            }
                        ]
                    }, {
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'textfield',
                                name : 'ssn',
                                fieldLabel: 'SSN',
                                value: 'XXX-XX-1234',
                                padding: '0 15 0 0',
                                width: 120
                            },{
                                xtype: 'datefield',
                                name : 'dob',
                                fieldLabel: 'DOB',
                                value: '01/01/1983',
                                padding: '0 40 0 0',
                                width: 110,
                                triggerCls: 'trgDate'
                            }
                        ]
                    }
                ]
            }
        ];
        this.callParent();
    }
});
