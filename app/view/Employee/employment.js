Ext.define("dst.view.Employee.employment", {
    extend: 'Ext.form.Panel',
    alias: 'widget.employment',
//    title: 'Employment',
    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'top',
        width: 140
    },
    border: false,
    autoHeight: true,
    bodyPadding: '0 10',
    initComponent: function(){
        this.items = [
            {
                layout: 'vbox',
                items: [{
                    layout: 'hbox',
                    items: [{
                        xtype: 'displayfield',
                        name : 'eligibleDate',
                        fieldLabel: 'Eligible Date',
                        value: '12/01/2007',
                        padding: '0 40 0 0'
                    },{
                        xtype: 'datefield',
                        name : 'preHireDate',
                        labelWidth: 110,
                        fieldLabel: 'Predecessor Hire:',
                        padding: '0 40 0 0',
                        width: 110,
                        triggerCls: 'trgDate'
                    },{
                        xtype: 'datefield',
                        name : 'hireDate',
                        fieldLabel: 'Hire Date',
                        padding: '0 40 0 0',
                        width: 110,
                        triggerCls: 'trgDate'
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'checkboxfield',
                        name : 'rehire',
                        boxLabel: 'Rehire',
                        boxLabelAlign: 'after'
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'combobox',
                        name : 'status',
                        fieldLabel: 'Status',
                        typeAhead: true,
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: [
                            ['Active','Active'],
                            ['Inactive','Inactive']
                        ],
                        lazyRender: true,
                        listClass: 'x-combo-list-small',
                        width: 180
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'textfield',
                        name : 'jobClassification',
                        fieldLabel: 'Job Classification',
                        value: 'Owner',
                        padding: '0 60 0 0'
                    },{
                        xtype: 'textfield',
                        name : 'employeeId',
                        fieldLabel: 'Employee ID',
                        value: '18'
                    }]
                },{
                    layout: 'hbox',
                    items: [{
                        xtype: 'combobox',
                        name : 'affiliate',
                        fieldLabel: 'Affiliate',
                        typeAhead: true,
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: [
                            ['Not Applicable','Not Applicable'],
                            ['Applicable','Applicable']
                        ],
                        lazyRender: true,
                        listClass: 'x-combo-list-small',
                        padding: '0 20 0 0',
                        width: 180
                    },{
                        xtype: 'combobox',
                        name : 'location',
                        fieldLabel: 'Location',
                        typeAhead: true,
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: [
                            ['Truck','Truck'],
                            ['Taxi','Taxi']
                        ],
                        lazyRender: true,
                        listClass: 'x-combo-list-small',
                        width: 180
                    }]
                }]
            }
        ];
        this.callParent();
    }
});