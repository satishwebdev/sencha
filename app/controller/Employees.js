Ext.define('dst.controller.Employees', {
    extend: 'Ext.app.Controller',
    models: ['Employees'],
    views: [
        'EmployeeForm'
    ]
});
