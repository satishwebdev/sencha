Ext.define('dst.model.Employees', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'firstName', type: 'string' },
        { name: 'lastName', type: 'string' },
        { name: 'birthDate', type: 'date' }
    ]
});
