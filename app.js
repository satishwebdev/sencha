Ext.require('Ext.container.Viewport');
Ext.require('Ext.form.Panel');
Ext.require('Ext.layout.container.Table');
Ext.application({
    name: 'dst',
    extend: 'dst.Application',
    appFolder: 'app',
    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'container',
            items: {
                layout: 'vbox',
                items: [{
                    xtype: 'employeeform'
                }]
            }
        });
    },
    controllers: [
        'Employees'
    ]
});
